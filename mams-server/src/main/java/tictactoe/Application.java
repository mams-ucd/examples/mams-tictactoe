package tictactoe;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.netty.handler.codec.http.HttpResponseStatus;
import mams.hypermedia.HALConstants;
import mams.hypermedia.HypermediaCollection;
import mams.hypermedia.JsonLdConstants;
import mams.hypermedia.Link;
import mams.hypermedia.transformer.HALTransformer;
import mams.hypermedia.transformer.HypermediaTransformer;
import mams.hypermedia.transformer.JsonLdTransformer;
import server.core.Path;
import server.core.ResponseEntity;
import server.core.Utils;
import server.core.WebServer;

public class Application {
    HypermediaCollection<Board> boards = new HypermediaCollection<>("boards");
    ObjectMapper mapper = new ObjectMapper();

    public static void main(String[] args) {
        new Application(8080);
    }

    public Application(int port) {
        HypermediaTransformer.registerTransformer(new HALTransformer());
        HypermediaTransformer.registerTransformer(new JsonLdTransformer());
        
        Utils.contentTypes.add(JsonLdConstants.CONTENT_TYPE);
        Utils.contentTypes.add(HALConstants.CONTENT_TYPE);
        Utils.defaultContentType = JsonLdConstants.CONTENT_TYPE;

        WebServer.getInstance(port);
        System.out.println("Server started on port: " + port);
        
        Link.baseUrl(WebServer.getInstance().getBaseUrl());
        boards.setLink(HALConstants.SELF, Link.absolute("/boards"));

        Path.create("/boards").post(state-> {
            try {
                Board board = mapper.readValue(Utils.getBody(state.request), Board.class);
                if (boards.get(board.id()) != null) {
                    WebServer.writeErrorResponse(state, HttpResponseStatus.CONFLICT);
                    return;
                }
    
                boards.add(board);
                WebServer.writeResponse(state,
                ResponseEntity.type(state.contentType)
                    .status(HttpResponseStatus.CREATED)
                    .location(board.link(HALConstants.SELF).href)
                );
            } catch (Exception e) {
                e.printStackTrace();
                WebServer.writeErrorResponse(state, HttpResponseStatus.INTERNAL_SERVER_ERROR);
            }
        }).get(state-> { 
            try {
                WebServer.writeResponse(state,
                ResponseEntity.type(state.contentType)
                    .status(HttpResponseStatus.OK)
                    .body(HypermediaTransformer.getTransformer(state.contentType).transform(boards))
                );
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                WebServer.writeErrorResponse(state, HttpResponseStatus.INTERNAL_SERVER_ERROR);
            }

        });
        Path.create("/boards/{id}").get(state-> {
            String id = state.binding.get("id");
            Board board = boards.get(id);
            try {
                WebServer.writeResponse(state,
                    ResponseEntity.type(state.contentType)
                        .status(HttpResponseStatus.OK)
                        .body(HypermediaTransformer.getTransformer(state.contentType).transform(board))
                );
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                WebServer.writeErrorResponse(state, HttpResponseStatus.INTERNAL_SERVER_ERROR);
            }
        });
        Path.create("/boards/{id}/{row}/{column}").put(state->{
            String id = state.binding.get("id");
            Board board = boards.get(id);
            if (board == null) {
                WebServer.writeErrorResponse(state, HttpResponseStatus.NOT_FOUND);
            } else {
                int row = Integer.parseInt(state.binding.get("row"));
                int column = Integer.parseInt(state.binding.get("column"));
                try {
                    String token = Utils.getBody(state.request);
                    if (board.board[row][column].equals("")) {
                        board.board[row][column] = token;
                        WebServer.writeErrorResponse(state, HttpResponseStatus.OK);
                    } else {
                        WebServer.writeErrorResponse(state, HttpResponseStatus.CONFLICT);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    WebServer.writeErrorResponse(state, HttpResponseStatus.INTERNAL_SERVER_ERROR);
                }
            }
        });
    }
}
