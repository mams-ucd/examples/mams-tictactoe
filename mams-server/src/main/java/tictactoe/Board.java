package tictactoe;

import mams.hypermedia.HypermediaObject;
import mams.hypermedia.HypermediaObject.Context;
import mams.hypermedia.HypermediaObject.Type;

@Context("http://ont.astralanguage.com/tictactoe")
@Type("Board")
public class Board extends HypermediaObject {
    public String id;
    public String[][] board = new String[3][3];

    public Board() {
        for (int i=0;i<3;i++)
            for (int j=0;j<3;j++)
                board[i][j]="";
    }

    @Override
    public String id() {
        return id;
    }
    
}
