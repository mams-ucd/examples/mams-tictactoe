package tictactoe.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import tictactoe.model.Board;

@RestController
public class BoardController {
    private Board board = new Board();
    
    @GetMapping("/board")
    public ResponseEntity<Board> getBoard() {
        return ResponseEntity.status(HttpStatus.OK).body(board);
    }

    @PutMapping("/board/{row}/{column}")
    public ResponseEntity<String> setToken(@PathVariable int row, @PathVariable int column, @RequestBody String token) {
        if (board.board[row][column].equals("")) {
            board.board[row][column] = token;
            return new ResponseEntity<String>(HttpStatus.OK);
        }
        return new ResponseEntity<String>(HttpStatus.CONFLICT);
    }
}
