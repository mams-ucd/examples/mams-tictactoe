# mams-tictactoe

Very simple Tic-Tac-Toe example. Currently just plays the game using the first available location on the board.  The server component implements a basic Tic-Tac-Toe board that exposes two endpoints:

* `/board`: GET request returns a list of lists of strings representing the current state of the board (200 OK)
* `/board/{row}/{column}`: PUT request places a token (defined as an 'X' or 'O' in the body of the request) at the specified location on the board. If successful, a 200 OK response is returned. If a token is already placed at the specified location, a 409 CONFLICT response is returned.


# Running with Maven:

Step 1: Start the TicTacToe server

```
mvn compile spring-boot:run -pl server
```

Step 2: Start the agents

```
mvn compile astra:deploy -pl agents
```

Step 3: query the game state in a browser

```
http://localhost:8080/board
```

# Running with Docker:

Step 1: Compile the source (from the root folder of the codebase):

```
mvn package
```

Step 2: Start Docker (if not already running)


Step 3: Deploy the system:

```
docker-compose up --build
```